<?php

namespace Drupal\views_infinite_scroll\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;

class PageViewController extends ControllerBase {
	public function storePageViewData() {
		$user = \Drupal::currentUser ();
		
		$n = $user->getAccountName ();
		
		$ip = \Drupal::request ()->getClientIp ();
		
		$pagename = \Drupal::request ()->get ( 'pagename' );
		$browser = \Drupal::request ()->get ( 'browser' );
		$language = \Drupal::request ()->get ( 'language' );
		
		db_insert ( 'page_info' )->fields ( array (
				'user_ip' => $ip,
				'pageview_count' => 1,
				'browser_name' => $browser,
				'browser_lang' => $language 
		) )->execute ();
		
		
		$num_updated = db_update('page_info')
		->fields(array(
				'user_ip' => $ip,
				'pageview_count' => 1,
				'browser_name' => $browser,
				'browser_lang' => $language 
		))
		->condition('user_ip', REQUEST_TIME - 3600, '>=')
		->execute();
		
		
		return [ ];
	}
	public function viewDB() {
		$rows = array ();
		$count = 0;
		$result = db_query ( "SELECT user_ip, pageview_count, browser_name, browser_lang FROM page_info" );
		
		foreach ( $result as $item ) {
			// $row = ( object ) [
			// 'ip' => $item->user_ip,
			// 'count' => $item->pageview_count,
			// 'name' => $item->browser_name,
			// 'lang' => $item->browser_lang
			// ];
			// $row->lang = $item->browser_lang;
			
			$rows [$count] ['ip'] = $item->user_ip;
			$rows [$count] ['count'] = $item->pageview_count;
			$rows [$count] ['name'] = $item->browser_name;
			$rows [$count] ['lang'] = $item->browser_lang;
			$count ++;
		}
		
	//	$encoded = json_encode ( $rows );
		
		return array (
				'#theme' => 'page_views',
				'rows' => $rows 
		);
	}
	function getAccessToken($code) {
		$redirecturl = MEMBERSHIP_APP . 'verifymeetup';
		$url = "https://secure.meetup.com/oauth2/access";
		$data = array (
				"client_id" => MEETUP_CONSUMER_KEY,
				"client_secret" => MEETUP_OAUTHSECRET,
				"grant_type" => "authorization_code",
				"redirect_uri" => $redirecturl,
				"code" => $code 
		);
		
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
				'Content-Type: application/x-www-form-urlencoded' 
		) );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_VERBOSE, 1 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
		
		$resp = curl_exec ( $ch );
		// drupal_set_message ( t ( 'The resp ' . $resp ) );
		curl_close ( $ch );
		
		$result = json_decode ( $resp );
		
		$accessToken = $result->access_token;
		// drupal_set_message ( t ( 'The accessToken ' . $accessToken ) );
		return $accessToken;
	}
	public function toMeetup() {
		$redirecturl = MEMBERSHIP_APP . 'verifymeetup';
		$url = "https://secure.meetup.com/oauth2/authorize?client_id=" . MEETUP_CONSUMER_KEY . "&response_type=code&redirect_uri=$redirecturl";
		return new TrustedRedirectResponse ( $url );
	}
}
