<?php

/**
 @file
 Contains \Drupal\onespirit\Controller\MembershipController.
 */
namespace Drupal\onespirit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;

define ( MEETUP_APIKEY, "461358c80146e6de1c27122a7765f" );
define ( MEETUP_CONSUMER_KEY, "iiefme6fo8haglcg7l7v8geakb" );
define ( MEETUP_OAUTHSECRET, "gk3uvjbnjnq3nejpgrcvpljb53" );
// define ( MEETUP_CONSUMER_KEY, "rnit6mq3hit5609h5ib9r2apri" );
// define ( MEETUP_OAUTHSECRET, "ireulrhj57uk6pglqsghape6nv" );
define ( MEMBERSHIP_APP, "http://membership.onespirit.co/membership/" );
class MembershipController extends ControllerBase {
	function getMeetupVars($meetup_id) {
		if ($meetup_id == "" || $meetup_id == null) {
			return "MU_EMTPY";
		}
		
		$meetupgroup = "OneSpirit-Ecstatic-Dance";
		$url = "api.meetup.com/2/members/?member_id=$meetup_id&key=" . MEETUP_APIKEY . "&group_urlname=$meetupgroup&sign=true";
		
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		$resp = curl_exec ( $ch );
		error_log ( date ( '[Y-m-d H:i e] ' ) . "Response from step 4: " . $resp . PHP_EOL, 3, LOG_FILE );
		curl_close ( $ch );
		$decoded = json_decode ( $resp, true );
		
		if ($decoded ['results'] [0] ['id'] == $meetup_id) {
			drupal_set_message ( t ( 'The meetup_name ' . $decoded ['results'] [0] ['name'] ) );
			$vars = array ();
			$vars ['name'] = $decoded ['results'] [0] ['name'];
			$vars ['photolink'] = $decoded ['results'] [0] ['photo'] ['photo_link'];
			return $vars;
		} else
			// not our group member
			return "MU_NOMEMBER";
	}
	public function meetupNotVerified() {
		return array (
				'#type' => 'markup',
				'#markup' => 'No Existo' 
		);
	}
	public function verifyMeetup() {
		$code = \Drupal::request ()->get ( 'code' );
		drupal_set_message ( t ( 'The code' . $code ) );
		
		// start the authentication
		$accessToken = $this->getAccessToken ( $code );
		if ($accessToken == null || empty ( $accessToken )) {
			return $this->redirect ( 'meetup.notverified' );
		}
		
		$id = $this->getMeetupId ( $accessToken );
		$name = $this->getMeetupVars ( $id ) ['name'];
		$photolink = $this->getMeetupVars ( $id ) ['photolink'];
		$tempstore = \Drupal::service ( 'user.private_tempstore' )->get ( 'onespirit' );
		$details = array ();
		$details ['meetup_id'] = $id;
		$details ['meetup_name'] = $name;
		$tempstore->set ( 'user_details', $details );
		return [ 
				'#theme' => 'meetup',
				'#meetup_name' => $name,
				'#meetup_id' => $id,
				'#photolink' => $photolink 
		];
	}
	function getAccessToken($code) {
		$redirecturl = MEMBERSHIP_APP . 'verifymeetup';
		$url = "https://secure.meetup.com/oauth2/access";
		$data = array (
				"client_id" => MEETUP_CONSUMER_KEY,
				"client_secret" => MEETUP_OAUTHSECRET,
				"grant_type" => "authorization_code",
				"redirect_uri" => $redirecturl,
				"code" => $code 
		);
		
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
				'Content-Type: application/x-www-form-urlencoded' 
		) );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_VERBOSE, 1 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
		
		$resp = curl_exec ( $ch );
		// drupal_set_message ( t ( 'The resp ' . $resp ) );
		curl_close ( $ch );
		
		$result = json_decode ( $resp );
		
		$accessToken = $result->access_token;
		// drupal_set_message ( t ( 'The accessToken ' . $accessToken ) );
		return $accessToken;
	}
	function getMeetupId($accessToken) {
		// with access token we are authorised to act on behalf of the client
		// self is alais for user id so we can get users details.
		$url2 = "https://api.meetup.com/2/member/self/?access_token=$accessToken";
		
		$ch2 = curl_init ( $url2 );
		curl_setopt ( $ch2, CURLOPT_RETURNTRANSFER, true );
		
		$resp2 = curl_exec ( $ch2 );
		curl_close ( $ch2 );
		// drupal_set_message ( t ( 'The resp2' . $resp2 ) );
		// convert from json to object
		$result2 = json_decode ( $resp2 );
		$id = $result2->id;
		// use the id of the overall meetup member to determine whether in our group
		return $id;
	}
	public function toMeetup() {
		$redirecturl = MEMBERSHIP_APP . 'verifymeetup';
		$url = "https://secure.meetup.com/oauth2/authorize?client_id=" . MEETUP_CONSUMER_KEY . "&response_type=code&redirect_uri=$redirecturl";
		return new TrustedRedirectResponse ( $url );
	}
	public function verifyEmail($hash) {
		$tempstore = \Drupal::service ( 'user.private_tempstore' )->get ( 'onespirit' );
		global $base_url;
		$_hash = $tempstore->get ( 'hash' );
		if ($hash == $_hash) {
			$url = $base_url . '/membership/verifymeetup';
			drupal_set_message ( t ( "Thank  you! Your email is confirmed! Please click on <a href='$url'>this link</a> to verify you are a member of our meetup group:" ) );
		} else {
			drupal_set_message ( t ( 'The codes do not match!' ) );
		}
		
		return array (
				'#type' => 'markup',
				'#markup' => $add_link 
		);
	}
	public function hello($name) {
		$name = \Drupal::request ()->get ( 'firstname' );
		// the {name} in the route gets captured as $name variable
		// in the function called
		return [ 
				'#theme' => 'hello_page',
				'#name' => $name,
				'#attached' => [ 
						'library' => [ 
								'acme/acme-styles' 
						] 
				] 
		]; // include our custom library for this response
	}
	public function content($request) {
		$firstname = $request->query->post ['firstname'];
		return [ 
				'#theme' => 'hello_page',
				'#name' => $firstname 
		];
	}
}
